
PROGRAM="$HOME/Programs"

# Go support
export GOROOT="$PROGRAM/go1.10"
export GOPATH="$PROGRAM/gopath"
PATH="$GOPATH/bin:$GOROOT/bin:$PATH"

# Java JDK support
export JAVA_HOME="$PROGRAM/jdk1.8.0_162"
PATH="$JAVA_HOME/bin:$PATH"

# maven support
PATH="$PROGRAM/apache-maven-3.5.3/bin:$PATH"

# gradle support
PATH="$PROGRAM/gradle-4.6/bin:$PATH"

# protobuf support
PATH="$PROGRAM/protoc-3.5.1/bin:$PATH"

export ENV="/home/jam/env"
