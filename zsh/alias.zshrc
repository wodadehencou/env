
alias work="cd ~/Workspace"
alias q="exit"
alias b="cd .."
alias view="vim -R"
alias vi="vim"
cdls(){chdir $1;ls}
alias cd='cdls'
#alias gvim="gvim -geom 160x80"
alias ssh='ssh -o serveraliveinterval=360 -X'

alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'
alias ls='ls -F --color=auto'
alias ll='ls -l'
alias la='ls -a'
alias grep='grep --color=auto'

alias format_json='python -mjson.tool'
