#!/bin/bash

stop() {
    # must interrupt it to save session
    killall -2 aria2c
    echo 'stop'
}
start(){
    aria2_conf_path=/home/jam/env/aria2           #配置文件目录
	echo $aria2_conf_path
    aria2_configfile="$aria2_conf_path/aria2.conf"      #配置
    aria2_downloadlist="$aria2_conf_path/aria2.session" #下载任务列表
    rpc_secret=""           #管理密码，1.19后使用

    [ ! -d "$aria2_conf_path" ] && mkdir -p "$aria2_conf_path"
    [ ! -f "$aria2_downloadlist" ] && touch "$aria2_downloadlist"
    cmd="aria2c -c -D --enable-rpc --rpc-listen-all=true --rpc-allow-origin-all --conf-path=$aria2_configfile \
    --http-accept-gzip=true ";
    [ "$rpc_secret" != "" ] && cmd=$cmd" --rpc-secret=$rpc_secret ";
    [ "`pgrep aria2c`" != "" ] && {
    stop
    sleep 3
    }
    echo 'starting...'
    eval "$cmd"
}
restart(){
    stop
    sleep 3 
    start
}

case "$1" in  
start)  
        start  
        ;;  
stop)  
        stop  
        ;;  
reload|force-reload|restart)  
        restart  
        ;;
status|pid)  
        pgrep aria2c  
        ;;
*)  
        echo "Usage: $0 {start|stop|restart|pid}"  
        exit 1  
esac 
